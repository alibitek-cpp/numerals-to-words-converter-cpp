# Numerals to Words Converter in C++

Numerals to Words Converter in C++ inspired from inflect https://pypi.python.org/pypi/inflect  

Correctly generate plurals, singular nouns, ordinals, indefinite articles; convert numbers to words.