#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <cstdint>
#include <deque>
#include <cmath>
#include <cinttypes>
using namespace std;

namespace NumeralsConverter {

static const char* ordinals[] = {
    "th", // 0
    "st", // 1
    "nd", // 2
    "rd", // 3
};

uint64_t denominations[] = {
    1000000000000000000, // quintillion, 18 zeros
    1000000000000000, // quadrillion. 15 zeros
    1000000000000, // trillion, 12 zeros
    1000000000, // billion, 9 zeros
    1000000, // million, 6 zeros
    1000, // thousand, 3 zeros
    100, // hundred, 2 zeros
    1 //
};

const int numberOfDenums = sizeof(denominations)/sizeof(uint64_t);
char ordinalWords[][20] = {	"first", 	"second", 		"third", 		"fourth", 		"fifth", 		"sixth", 		"seventh", 		"eighth", 	"ninth", "tenth", "eleventh",
                            "twelfth", 	"thirteenth", 	"fourteenth", 	"fifteenth", 	"sixteenth", 	"seventeenth", 	"eighteenth", 	"nineteenth" };

char Ones[][20] = {"zero ", "one ", "two ", "three ", "four ", "five ", "six ", "seven ", "eight ", "nine "};
char Tens[][20] = {"twenty ", "thirty ", "forty ", "fifty ", "sixty ", "seventy ", "eighty ", "ninety "};
char Scale[][20] = {"quintillion ", "quadrillion ", "trillion ", "billion ", "million ", "thousand ", "hundred ", ""};
char Teens[][20] = {"ten ", "eleven ", "twelve ", "thirteen ", "fourteen ", "fifteen ", "sixteen ", "seventeen ", "eighteen ", "nineteen "};

int lastDigits(int number, int n = 1) {
    return number % (10 * n);
}

uint16_t numberOfDigits(uint64_t x)
{
    x = abs(x);

    return  (x < 10 ? 1 :
            (x < 100 ? 2 :
            (x < 1000 ? 3 :
            (x < 10000 ? 4 :
            (x < 100000 ? 5 :
            (x < 1000000 ? 6 :
            (x < 10000000 ? 7 :
            (x < 100000000 ? 8 :
            (x < 1000000000 ? 9 :
            (x < 10000000000 ? 10 :
            (x < 100000000000 ? 11 :
            (x < 1000000000000 ? 12 :
            (x < 10000000000000 ? 13 :
            (x < 100000000000000 ? 14 :
            (x < 1000000000000000 ? 15 :
            (x < 10000000000000000 ? 16 :
            (x < 100000000000000000 ? 17 :
            18)))))))))))))))));
}

/**
     * @brief NumberToOrdinal The ordinal of the integer number, e.g. ordinal(1) returns '1st'
     * @param number
     * @return
     */
string ordinal(uint64_t number, bool withNumber = true) {
    int last = lastDigits(number);

    char numberbuffer[256];
    snprintf(numberbuffer, sizeof numberbuffer, "%" PRIu64, number);

    if (lastDigits(number, 2) - last == 10) {       
        return withNumber ? string(numberbuffer) + ordinals[0] : ordinals[0];
    } else if (last < 4){
        return withNumber ? string(numberbuffer) + ordinals[last] : ordinals[last];
    } else {
        return withNumber ? string(numberbuffer) + ordinals[0] : ordinals[0];
    }
}

/**
     * @brief NumberToOrdinal The ordinal of the text number, e.g. ordinal("one") returns 'first'
     * @param number
     * @return
     */
string ordinal(string number) {
    map<string, string> ordinals;
    ordinals["ty"]="tieth";
    ordinals["one"]="first";
    ordinals["two"]="second";
    ordinals["three"]="third";
    ordinals["five"]="fifth";
    ordinals["eight"]="eighth";
    ordinals["nine"]="ninth";
    ordinals["twelve"]="twelfth";

    for (auto it = ordinals.rbegin(); it != ordinals.rend(); ++it) {
        string::size_type pos;
        if ((pos = number.find(it->first)) != string::npos) {
            return number.replace(pos, it->first.length(), it->second);
        }
    }

    return number + "th";
}

string UnderHundred(uint64_t n, bool appendHundred = true, bool appendZero = true)
{
    int n1, n2, n3;
    n1 = n % 10; n /= 10;
    n2 = n % 10; n /= 10;
    n3 = n % 10;

    string s = "";

    if (n3 != 0)
    {
        s += Ones[n3];
        if (appendHundred)
            s += "hundred ";
    }

    if (n2 != 0)
    {
        if (n2 == 1)
        {
            s += Teens[n1];
            return s;
        }

        s += Tens[n2 - 2];
        s.replace(s.length() - 1, 1, "-");
    }

    if (n1 != 0 || appendZero)
    {
        s += Ones[n1];
    }
    else
    {
        if (s[s.length() - 1] == '-')
            s.replace(s.length() - 1, 1, "");
    }
    return s;
}

enum EFormat {
    kDecimal,
    kOrdinal
};

template < class ContainerT >
void tokenize(const std::string& str, ContainerT& tokens,
              const std::string& delimiters = " ", bool trimEmpty = false)
{
    std::string::size_type pos, lastPos = 0;

    using value_type = typename ContainerT::value_type;
    using size_type  = typename ContainerT::size_type;

    while(true)
    {
        pos = str.find_first_of(delimiters, lastPos);
        if(pos == std::string::npos)
        {
            pos = str.length();

            if(pos != lastPos || !trimEmpty)
                tokens.push_back(value_type(str.data()+lastPos,
                                            (size_type)pos-lastPos ));

            break;
        }
        else
        {
            if(pos != lastPos || !trimEmpty)
                tokens.push_back(value_type(str.data()+lastPos,
                                            (size_type)pos-lastPos ));
        }

        lastPos = pos + 1;
    }
}

bool isOdd(uint64_t number) {
    return number % 2 == 1;
}

bool isEven(uint64_t number) {
    return number % 2 == 0;
}

int getDigitsLeftToRight(uint64_t from, uint16_t index, uint64_t groupSize = 1)
{
    if (numberOfDigits(from) < groupSize)
        return from;

    if (groupSize == 0)
        groupSize = 1;

    uint64_t groupScale = pow(10, groupSize);

    return (from / (int)pow(10, floor(log10(from)) - index)) % groupScale;
}

int getDigitsRightToLeft(uint64_t from, uint16_t index, uint64_t groupSize = 1)
{
    if (numberOfDigits(from) < groupSize)
        return from;

    if (groupSize == 0)
        groupSize = 1;

    uint64_t groupScale = pow(10, groupSize);

    return (from / (int)pow(10, index)) % groupScale;
}

enum EDirection {
    kLeftToRight,
    kRightToLeft
};

deque<uint64_t> getGroups(uint64_t number, uint16_t groupSize, EDirection direction = kLeftToRight) {
    if (!groupSize)
        throw 42;

    deque<uint64_t> groups;
    uint16_t nDigits = numberOfDigits(number);

    switch (groupSize)
    {
    case 1:
        for (uint16_t i = 0; i < nDigits; i++)
        {
            uint64_t group;

            switch (direction)
            {
            case kLeftToRight:
                group = getDigitsLeftToRight(number, i);
                break;
            case kRightToLeft:
                group = getDigitsRightToLeft(number, i);
                break;
            }

            groups.push_back(group);
        }
        break;
    case 2:
        switch (direction)
        {
        case kLeftToRight:
            for (uint16_t i = 1; i < nDigits; i += 2)
            {
                groups.push_back(getDigitsLeftToRight(number, i, groupSize));
            }

            if (isOdd(nDigits))
            {
                groups.push_back(number % 10);
            }
            break;
        case kRightToLeft:
            for (uint16_t i = 0; i < nDigits; i += 2)
            {
                groups.push_back(getDigitsRightToLeft(number, i, groupSize));
            }
            break;
        }
        break;
    case 3:
        switch (direction)
        {
        case kLeftToRight:
            for (uint16_t i = 2; i < nDigits; i += 3)
            {
                groups.push_back(getDigitsLeftToRight(number, i, 3));
            }

            if (isEven(number))
            {
                if (nDigits > groupSize)
                    groups.push_back(number % 10);
                else
                    groups.push_back(number);
            }
            else if (nDigits > 3)
                groups.push_back(number % 100);
            break;

        case kRightToLeft:
            for (uint16_t i = 0; i < nDigits; i += 3)
            {
                groups.push_back(getDigitsRightToLeft(number, i, 3));
            }

            break;
        }
        break;
    }

    return groups;
}

string numberToWord(uint64_t number, EFormat format = kDecimal, bool appendScale = true, bool appendZero = false, bool appendAnd = true)
{
    uint64_t myNumber = number;
    uint64_t myDigits = numberOfDigits(number);

    if (number == 0)
        return Ones[0];

    if (number > 0xFFFFFFFFFFFFFFFF)
        throw 42;

    int denumCount[numberOfDenums] = {0};
    int count = 0;

    while (true)
    {
        if (number < denominations[count])
        {
            count++;

            if (count >= numberOfDenums)
            {
                break;
            }

            continue;
        }

        if (count == numberOfDenums){
            denumCount[count] += number;
            number -= denominations[count];
        } else {
            denumCount[count]++;
            number -= denominations[count];
        }
    }

    string s = "";

    for (int i = 0; i < numberOfDenums; i++)
    {
        if (denumCount[i] == 0)
            continue;

        s += UnderHundred(denumCount[i], appendScale, appendZero);
        if (appendScale)
            s += Scale[i];

        if (i < numberOfDenums - 2)
            s.replace(s.length() - 1, string::npos, ", ");
        else if ((myNumber % (uint64_t)pow(10, myDigits - 1) != 0) && i < numberOfDenums - 1 && appendAnd)
            s.replace(s.length() - 1, string::npos, " and ");
        else if ((myNumber % (uint64_t)pow(10, myDigits - 1) == 0) && i < numberOfDenums - 1)
            s.replace(s.length() - 1, string::npos, "");
    }

    if (format == kOrdinal)
    {
        string::size_type pos;
        if ((pos = s.rfind('-')) != string::npos) {
            string lastWord = s.substr(pos);
            std::size_t first = lastWord.find_first_not_of(' ');
            std::size_t last = lastWord.find_last_not_of(' ');
            lastWord = lastWord.substr(first, (last - first + 1));
            s.replace(pos, string::npos, ordinal(lastWord));
        } else {
            vector<string> tokens;
            tokenize(s, tokens, " ", true);
            vector<string>::const_reference lastWord = tokens.back();
            string::size_type pos;
            if ((pos = s.rfind(lastWord)) != string::npos) {
                string ord = ordinal(lastWord);
                s.replace(pos, string::npos, ord);
            }
        }

    }

    return s;
}


vector<string> numberToWordsGrouping(uint64_t number, uint16_t groupSize = 0,
                                     EFormat format = kDecimal,
                                     EDirection direction = kLeftToRight,
                                     bool appendScale = true,
                                     bool appendAnd = true,
                                     bool appendZero = true) {
    if (groupSize > 3)
        throw 42;

    vector<string> output;

    if (groupSize == 0) {
        output.push_back(numberToWord(number, format));
    } else {
        deque<uint64_t> groups = getGroups(number, groupSize, direction);
        size_t sz = groups.size();

        for (size_t i = 0; i < sz; ++i)
        {
            auto group = groups.at(i);

            if (i != sz - 1 && numberOfDigits(group) == 1) {
                output.push_back((!group ? "" : (Ones[0])) + numberToWord(group, format, appendScale, !group ? false : appendZero, appendAnd));
            }
            else
            {
                if (output.size() && output.back().substr(output.back().length() - 1, string::npos) != " ")
                    output.back() = output.back() + " ";
                output.push_back(numberToWord(group, format, appendScale, appendZero, appendAnd));
            }
        }
    }

    return output;
}


bool isOrdinal(string number) {
    string numberPart = number.substr(0, number.length() - 2);
    return number == ordinal(strtoull(&numberPart[0], (char **)NULL, 10));
}

string ordinalToWord(string number, EFormat format = kOrdinal, bool appendZero = false, bool appendScale = true, bool appendAnd = true) {
    string numberPart = number.substr(0, number.length() - 2);
    return numberToWord(strtoull(numberPart.data(), (char **)NULL, 10), format, appendScale, appendZero, appendAnd);
}

}

int main() {
    uint64_t n = 6309123;
    uint16_t nDigits = NumeralsConverter::numberOfDigits(n);
    cout << "n=" << n << " " << "Number of digits: " << nDigits << endl;

    cout << "Digits Left to Right: ";
    for (uint8_t i = 0; i < nDigits; ++i) {
        cout << NumeralsConverter::getDigitsLeftToRight(n, i, 2) << " ";
    }

    cout << "\nDigits Right to Left: ";
    for (uint8_t i = 0; i < nDigits; ++i) {
        cout << NumeralsConverter::getDigitsRightToLeft(n, i, 2) << " ";
    }
    cout << endl;

    cout << NumeralsConverter::numberToWord(1213, NumeralsConverter::kOrdinal) << endl;

    for (const auto& group : NumeralsConverter::numberToWordsGrouping(n, 3)) {
        cout << group << endl;
    }

    for (const auto& group : NumeralsConverter::numberToWordsGrouping(6309123, 1)) {
        cout << group << endl;
    }

    for (const auto& group : NumeralsConverter::numberToWordsGrouping(6309123, 2)) {
        cout << group << endl;
    }
    cout << NumeralsConverter::numberToWord(1201) << endl;
    cout << NumeralsConverter::numberToWord(1202, NumeralsConverter::kOrdinal) << endl;

    for (int i = 1; i < 1230; ++i) {
        cout << NumeralsConverter::ordinal(i) << " " << NumeralsConverter::numberToWord(i, NumeralsConverter::kOrdinal) << endl;
    }

    cout
            << NumeralsConverter::ordinal("one") << " "
            << NumeralsConverter::ordinal("two") << " "
            << NumeralsConverter::ordinal("three") << " "
            << NumeralsConverter::ordinal("four") << " "
            << NumeralsConverter::ordinal("five") << " "
            << NumeralsConverter::ordinal("six") << " "
            << NumeralsConverter::ordinal("seven") << " "
            << NumeralsConverter::ordinal("eight") << " "
            << NumeralsConverter::ordinal("nine") << " "
            << NumeralsConverter::ordinal("thirty") << endl;

    for (int i = 1; i < 2000; ++i) {
        cout << i << NumeralsConverter::ordinal(i) << " " << NumeralsConverter::numberToWord(i) << endl;
    }

    cout << NumeralsConverter::numberToWord(1234) << "\n"
         << NumeralsConverter::numberToWord(1234, NumeralsConverter::kOrdinal) << endl;

    for (const auto& group : NumeralsConverter::numberToWordsGrouping(n, 3, NumeralsConverter::kDecimal, NumeralsConverter::kRightToLeft)) {
        cout << group << endl;
    }

    for (const auto& group : NumeralsConverter::numberToWordsGrouping(1234, 3, NumeralsConverter::kDecimal, NumeralsConverter::kLeftToRight)) {
        cout << group;
    }

    cout << endl;

    for (const auto& group : NumeralsConverter::numberToWordsGrouping(12345, 3, NumeralsConverter::kDecimal, NumeralsConverter::kLeftToRight)) {
        cout << group;
    }

    cout << endl;

    for (const auto& group : NumeralsConverter::numberToWordsGrouping(12, 3, NumeralsConverter::kDecimal, NumeralsConverter::kLeftToRight)) {
        cout << group;
    }

    cout << endl;

    for (const auto& group : NumeralsConverter::numberToWordsGrouping(123, 3, NumeralsConverter::kDecimal, NumeralsConverter::kLeftToRight, false, false)) {
        cout << group;
    }

    cout << endl;

    for (const auto& group : NumeralsConverter::numberToWordsGrouping(123, 3, NumeralsConverter::kDecimal, NumeralsConverter::kLeftToRight)) {
        cout << group;
    }

    cout << endl;

    cout
            << "1st " << boolalpha << NumeralsConverter::isOrdinal("1st") << "\n"
               << "4st " << boolalpha << NumeralsConverter::isOrdinal("4st") << "\n"
                  << "Wrd " << boolalpha << NumeralsConverter::isOrdinal("Wrd") << "\n"
                     << "5rd " << boolalpha << NumeralsConverter::isOrdinal("5rd") << "\n"
                    << "3rd " << boolalpha << NumeralsConverter::isOrdinal("3rd") << "\n";

    for (int i = 1; i <= 20; i++)
    {
        cout << i << " " << NumeralsConverter::ordinalToWord(NumeralsConverter::ordinal(i), NumeralsConverter::kOrdinal, true) << '\n';
    }
    cout << endl;

    for (int i = 1; i <= 20; i++)
    {
        cout << i << " " << NumeralsConverter::ordinalToWord(NumeralsConverter::ordinal(i), NumeralsConverter::kOrdinal, false) << '\n';
    }
    cout << endl;

    cout << NumeralsConverter::numberToWord(20) << '\n' << NumeralsConverter::numberToWord(20, NumeralsConverter::kOrdinal) << endl;

    for (int i = 1; i <= 501; i++)
        {
            cout << i << " | " << NumeralsConverter::ordinal(i) << " | " << NumeralsConverter::ordinalToWord(
                        NumeralsConverter::ordinal(i), NumeralsConverter::kOrdinal, false, true, true) << '\n';
        }

    cout << "\"" << NumeralsConverter::ordinalToWord(NumeralsConverter::ordinal(490), NumeralsConverter::kOrdinal, false, true, true) << "\"" << endl;
    cout << "\"" << NumeralsConverter::ordinalToWord(NumeralsConverter::ordinal(490), NumeralsConverter::kDecimal, true, true, true) << "\"" << endl;
  cout << "\"" << NumeralsConverter::ordinalToWord(NumeralsConverter::ordinal(500), NumeralsConverter::kOrdinal, false, true, true) << "\"" << endl;
    cout << "\"" << NumeralsConverter::numberToWord(500) << "\"";

    cout << "\"" << NumeralsConverter::numberToWord(490, NumeralsConverter::kOrdinal) << "\"" << endl;
    cout << NumeralsConverter::ordinal("ninety") << endl;

    for (const auto& group : NumeralsConverter::numberToWordsGrouping(10021, 2)) {
          cout << group << endl;
      }

      cout << endl;
      for (const auto& group : NumeralsConverter::numberToWordsGrouping(1001, 3)) {
          cout << group;
      }
      cout << endl;

        cout << endl;
      for (const auto& group : NumeralsConverter::numberToWordsGrouping(10020021, 2)) {
            cout << group << endl;
        }

      cout << endl;
    for (const auto& group : NumeralsConverter::numberToWordsGrouping(10020021, 3)) {
          cout << group << endl;
      }

    cout << endl;
  for (const auto& group : NumeralsConverter::numberToWordsGrouping(1004)) {
        cout << group << endl;
    }

    return 0;
}

